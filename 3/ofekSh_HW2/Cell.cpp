#include "Cell.h"
#define GLUCOSE_LEVEL 50

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_nucleus.init(dna_sequence);

	_glocus_receptor_gene.setStart(glucose_receptor_gene.get_start());
	_glocus_receptor_gene.setEnd(glucose_receptor_gene.get_end());
	_glocus_receptor_gene.set_is_on_complementary_dna_strand(glucose_receptor_gene.is_on_complementary_dna_strand());

	_mitochondrion.init();
}

bool Cell::get_ATP()
{
	std::string rna_transcript = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = _ribosome.create_protein(rna_transcript);
	if (protein == nullptr)
	{
		std::cerr << "ERROR: could not create a protein." << std::endl;
		_exit(1);
	}
	
	_mitochondrion.insert_glucose_receptor(*protein);
	_mitochondrion.set_glucose(GLUCOSE_LEVEL);
	if (!(_mitochondrion.produceATP(GLUCOSE_LEVEL)))
	{
		return false;
	}
	_atp_units = 100;
	return true;

}
