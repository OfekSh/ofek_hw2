#include <iostream>
#include "Nucleus.h"
#include <string.h>

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}


unsigned int Gene::get_start() const
{
	return _start;
}

unsigned int Gene::get_end() const
{
	return _end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

void Gene::setStart(const unsigned int start)
{
	_start = start;
}

void Gene::setEnd(const unsigned int end)
{
	_end = end;
}

void Gene::set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	unsigned int i = 0;
	bool flag = true;
	_DNA_strand = dna_sequence;
	for (i = 0; i < dna_sequence.size() && flag; i++)
	{
		if (dna_sequence[i] == (const char)'A')
		{
			_complementary_DNA_strand += "T";
		}
		else if (dna_sequence[i] == (const char)'T')
		{
			_complementary_DNA_strand += "A";
		}
		else if (dna_sequence[i] == (const char)'G')
		{
			_complementary_DNA_strand += "C";
		}
		else if (dna_sequence[i] == (const char)'C')
		{
			_complementary_DNA_strand += "G";
		}
		else
		{
			flag = false;
		}
	}
	//Need to add something. std::cerr..?????
	if (!flag)
	{
		std::cerr << "ERROR: wrong DNA pattern (Something else but the letters /'A/', /'G/', /'C/' or /'T/' was given.)" << std::endl;
		_exit(1);
	}

}

std::string Nucleus::get_RNA_transcript(const Gene & gene) const
{
	std::string RNA;
	std::string temp;
	unsigned int i = 0;

	if (gene.is_on_complementary_dna_strand())
	{
		temp = _complementary_DNA_strand;
	}
	else
	{
		temp = _DNA_strand;
	}

	for (i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (temp[i] == (const char)'T')
		{
			RNA += "U";
		}
		else
		{
			RNA += temp[i];
		}
	}
	return RNA;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed = _DNA_strand;

	std::reverse(reversed.begin(), reversed.end());

	return reversed;
}

/*
I used the help of "Stack overflow" here.
Link: https://stackoverflow.com/questions/22489073/counting-the-number-of-occurrences-of-a-string-within-a-string
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string & codon) const
{
	int occurences = 0;
	std::string::size_type pos = 0;

	while ((pos = _DNA_strand.find(codon, pos)) != std::string::npos)
	{
		occurences++;
		pos += codon.length();
	}

	return occurences;
}
