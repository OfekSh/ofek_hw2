#pragma once
#include "Protein.h"
#define CODON_SIZE 3

class Ribosome
{
public:
	Protein* create_protein(std::string &RNA_transcript) const;
};

