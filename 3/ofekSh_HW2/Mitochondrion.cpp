#include "Mitochondrion.h"
#include <iostream>
#include <String.h>

void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* curr = protein.get_first();
	int index = 0;
	bool isGood = true;

	while (isGood && curr)
	{
		if (index == 0)
		{
			if (curr->get_data() != ALANINE)
			{
				isGood = !isGood;
			}
		}
		else if (index == 1)
		{
			if (curr->get_data() != LEUCINE)
			{
				isGood = !isGood;
			}
		}
		else if (index == 2)
		{
			if (curr->get_data() != GLYCINE)
			{
				isGood = !isGood;
			}
		}
		else if (index == 3)
		{
			if (curr->get_data() != HISTIDINE)
			{
				isGood = !isGood;
			}
		}
		else if (index == 4)
		{
			if (curr->get_data() != LEUCINE)
			{
				isGood = !isGood;
			}
		}
		else if (index == 5)
		{
			if (curr->get_data() != PHENYLALANINE)
			{
				isGood = !isGood;
			}
		}

		index++;
		curr = curr->get_next();
	}

	if (isGood)
	{
		_has_glocuse_receptor = true;
	}
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	if(_has_glocuse_receptor && _glocuse_level >= (unsigned int)glocuse_unit)
	{
		return true;
	}
	return false;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}
