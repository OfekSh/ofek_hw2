#include "Ribosome.h"
#include <iostream>
#include <String.h>

Protein * Ribosome::create_protein(std::string & RNA_transcript) const
{
	size_t size = RNA_transcript.length();
	
	std::string temp;
	Protein *protein = new Protein;
	AminoAcid acid;

	protein->init();

	while (size >= CODON_SIZE)
	{
		temp = RNA_transcript.substr(0, CODON_SIZE);
		RNA_transcript = RNA_transcript.substr(CODON_SIZE, RNA_transcript.length());

		acid = get_amino_acid(temp);
		if (acid != UNKNOWN)
		{
			protein->add(acid);
		}
		else
		{
			size = -1;
			protein->clear();
		}
		size -= CODON_SIZE;
	}

	return protein;
}
